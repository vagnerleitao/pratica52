
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {
        private Double A;
        private Double B;
        private Double C;
        private static final String MSG = "Coeficiente a não pode ser zero";
        
    public static void main(String[] args) {
        //System.out.println("Olá, Java!");
              
        try{
        Equacao2Grau busca= new Equacao2Grau();
        Pratica52 coef=new Pratica52();
        busca.setA(coef.A);
        busca.setB(coef.B);
        busca.setC(coef.C);
        Object valor = busca.getA();
        Object valor1 = busca.getB();
        Object valor2 = busca.getC();
        Object raiz1 = busca.getRaiz1();
        Object raiz2 = busca.getRaiz2();
        System.out.println("valor a : "+valor+"\nvalor b : "+valor1+"\nvalor c : "+valor2);
        if(raiz1.equals(raiz2)) System.out.println("As Raizes são iguais");
        else System.out.println("As Raizes são diferentes");
        System.out.println("Raiz 1 : "+raiz1);
        System.out.println("Raiz 2 : "+raiz2);
        }//catch (NullPointerException npex) { System.out.println(npex.getLocalizedMessage()); }
        catch (RuntimeException rex) { System.out.println(rex.getLocalizedMessage());}

        
    }
    
    public Pratica52() throws RuntimeException {
        A=1.0;
        B=-6.0;
        C=1.0;
        if(A == 0.0 || A==null) throw new RuntimeException(MSG);
    }
    
}
