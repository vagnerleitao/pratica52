/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author vagner
 */
//public class Equacao2Grau<T extends Number & Comparable> {
public class Equacao2Grau<T extends Number & Comparable<? super Number>> {

        private T a;
        private T b;
        private T c;
        private double raiz1;
        private double raiz2;
        private double delta;
        private static final String MSG = "Coeficiente a não pode ser zero";
        private static final String MSG1 = "Equação não tem solução real";
        
    public Equacao2Grau(){
        
    }    
    
    public Equacao2Grau(T a, T b, T c) throws RuntimeException {
        if( a.doubleValue() == 0.0 || a==null ){
            throw new RuntimeException(MSG);
        }
        this.a=a;
        this.b=b;
        this.c=c;
    }
    
    public void setA(T a) throws RuntimeException {
        if(a.doubleValue() == 0.0 || a==null){
            throw new RuntimeException(MSG);
        }
        this.a = a;
    }

    public T getA(){
        return this.a;
    }
    
    public void setB(T b){
        this.b = b;
    }
    
    public T getB(){
        return this.b;
    }
    
        public void setC(T c){
        this.c = c;
    }
    
    public T getC(){
        return this.c;
    }
    
    public double getRaiz1()throws RuntimeException {
        if(this.a.doubleValue() == 0.0 || this.a==null){
            throw new RuntimeException(MSG);
        }
        delta = Math.pow(this.b.doubleValue(),2)-4*this.a.doubleValue()*this.c.doubleValue();
        if(delta < 0){
            throw new RuntimeException(MSG1);
        }
        raiz1=(-this.b.doubleValue()+Math.sqrt(delta))/2;
        //raiz1=(double)Math.round(raiz1*1000)/1000;
        return raiz1;
    }
    
    public double getRaiz2() throws RuntimeException{
        if(this.a.doubleValue() == 0.0 || this.a==null){
            throw new RuntimeException(MSG);
        }
        delta = Math.pow(this.b.doubleValue(),2)-4*this.a.doubleValue()*this.c.doubleValue();
        if(delta < 0){
            throw new RuntimeException(MSG1);
        }
        raiz2=(-this.b.doubleValue()-Math.sqrt(delta))/2;
        //raiz2=(double)Math.round(raiz2*1000)/1000;
        return raiz2;
    }
    
}
